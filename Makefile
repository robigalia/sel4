#
# Copyright 2014, NICTA
#
# This software may be distributed and modified according to the terms of
# the BSD 2-Clause license. Note that NO WARRANTY is provided.
# See "LICENSE_BSD2.txt" for details.
#
# @TAG(NICTA_BSD)
#

PHONY += kernel_elf_objcopy

# The main target we want to generate
all: kernel_elf_objcopy

kernel_elf_objcopy: kernel_elf
	$(Q)objcopy -O elf32-i386 "$(STAGE_BASE)/kernel.elf" "$(STAGE_ROOT)/kernel-$(ARCH_NAME)-$(PLAT)"

-include ${CONFIGFILE}

include tools/common/project.mk

# objdump args
ifeq (${OBJFLAGS}y,y)
    OBJFLAGS="Dlx"
endif

# objdump the kernel image
objdump-kernel:
	${CONFIG_CROSS_COMPILER_PREFIX}objdump -${OBJFLAGS} stage/${ARCH}/${PLAT}/kernel.elf
