# seL4 kernel build and configuration

This repository helps with building compatible configurations of the seL4
kernel for Robigalia. Its structure is similar to sel4test, but doesn't
contain any of the userspace C libraries.

The submodules here should be kept in careful sync with sel4-sys, especially
the kernel submodule.

# Building the kernel

Look at the `configs` directory, and run `make $PLAT_defconfig`. For example,
`make x64_qemu_defconfig`.

You can then run `make` and the kernel will be built into `stage/`.

# Configuring the kernel

By default, debug printing is enabled for the kernel.

You can tweak the kernel config with `make menuconfig`. Be warned that some
settings may break the Rust userspace support! In particular, for x64:

- `XSAVE_SIZE` of less than 576 will break creating TCBs
- `SYSCALL=n` and `SYSENTER=y` will break all syscalls.

To support AVX (and later) instructions on x64 these configurations must be set:

- One of the XSAVE instructions must be used. The selected XSAVE instruction
  must be supported on the target processor.
- `XSAVE_SIZE` must match or exceed the target processor and meet its alignment
  requirements. If using the cpuid package installed on a linux machine, you can
  find this with `cpuid | grep 'bytes required by XSAVE'`
- `XSAVE_FEATURE_SET` must be set to the feature flags you want to enable in
  XCR0. See the intel architecture software development manual 3A for possible
  values. These flags must be supported by your target processor. If using the
  cpuid package installed on a linux machine, you can find the flags supported
  by your processor with `cpuid | grep 'XCR0 [lower|upper]'`

The rust target file for x64 robigalia on sel4 does not generate AVX or later
instructions (see the sel4-targets project). You will need to modify that file
or override those settings with command line arguments to rustc to generate
those instructions.

Note that enabling and using AVX instructions is currently untested.

For ARM: we don't support ARM yet.

# Platforms

Currently supported platforms are x64 "PC99".

There is a config for x86 PC99, but it is completely untested, since we don't
support 32-bit at all anymore.
